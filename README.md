# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

|  Basic components   | Score | Check |
|:-------------------:|:-----:|:-----:|
| Basic control tools |  30%  |   Y   |
|     Text input      |  10%  |   Y   |
|     Cursor icon     |  10%  |   Y   |
|   Refresh button    |  10%  |   Y   |



|     Advanced tools     | Score | Check |
|:----------------------:|:-----:|:-----:|
| Different brush shapes |  15%  |   Y   |
|    Un/Re-do button     |  10%  |   Y   |
|       Image tool       |  5%   |   Y   |
|        Download        |  5%   |   Y   |

|   Other useful widgets   | Score | Check |
|:------------------------:|:-----:|:-----:|
| Brush shpae 實心空心切換 | 1~5%  |   Y   |
|          彩虹筆          | 1~5%  |   Y   |
|     清除選定矩形面積     | 1~5%  |   Y   |
| Brush shape - line shape | 1~5%  |   Y   |
|        漸層色方塊        | 1~5%  |   Y   |

---

### How to use

首先貼上website全圖：
![](https://i.imgur.com/HuFZjKA.png)


* 左上角為標題圖片，下圖紅圈處：
![](https://i.imgur.com/EKtl4zk.jpg)


* 左下角為canvas畫布，可以在此畫圖，下圖紅圈處：
![](https://i.imgur.com/UguNdh9.jpg)


* 右上角處，左邊可以選擇畫筆顏色，最左方可以選擇色相，再從中間的漸層色調色選擇色彩，也可以只選擇色相中的顏色，選擇完顏色的rgb以及hex value會顯示在中間上方的rgb、hex欄位，如下圖紅圈。
* 中間下方font type處可以選擇字體以及文字大小，如下圖綠圈。
* 右邊上方可以自訂輸出圖檔的檔名以及格式（jpg、png、jpeg)，如下圖藍圈。![](https://i.imgur.com/BlUnfCC.jpg)


* 右下角處為功能選擇以及筆刷大小的調整，如下圖紅圈處：
![](https://i.imgur.com/6VbPd7T.jpg)


* 由左到右，由上到下，分別標上1~16的編號，如下圖：
![](https://i.imgur.com/2RURCBv.jpg)

* mode 1為筆刷功能，可依據筆刷大小(Brush Size)以及筆刷顏色在畫布上繪製
* mode 2為橡皮擦功能，可依據筆刷大小在畫布上擦掉已畫上的圖案
* mode 3為文字功能，可依據右上角調整的Font type輸入文字，在畫布上點一下會跳出文字框，點擊文字框後開始輸入，輸入完畢再點擊一次畫布
* mode 4為circle brush shape，可在畫布上畫出圓形
* mode 5為square brush shape，可在畫布上畫出矩形
* mode 6為triangle brush shape，可在畫布上畫出三角形
* mode 7為brush shape的實心空心調整，可以讓畫出的圓形有實心跟空心的區別，mode 4、5、6、7如下圖：
![](https://i.imgur.com/oSFqqji.png)

* mode 8為refresh，按下可以清空畫布上的內容
* mode 9為un-do，按下可以復原成上一步
* mode 10為re-do，按下可以在復原後返回下一步
* mode 11為download，按下可以把畫布的內容依據右上角所輸入的檔名以及格式下載下來
* mode 12為image tool，按下後可以把圖片上傳並貼上畫布
* mode 13為彩虹筆，與筆刷類似，但顏色會如同彩虹般變換，如下圖：
![](https://i.imgur.com/FQVZyuq.png)
* mode 14為清除選定矩形面積，可根據拉出的矩形面積清除畫布上的內容
* mode 15為line brush shape，可依據筆刷大小在畫布上畫出線條。
* mode 16為漸層色矩形，可在畫布上畫出漸層色的矩形，如下圖：
![](https://i.imgur.com/PwB82bj.png)
* 最下方處為Brush size，可調整筆刷的大小，下圖紅圈處：
![](https://i.imgur.com/rJZvGsD.jpg)

* Cursor icon的部分，選定模式移入畫布便會顯示不同的cursor

---
### Function description


#### Bonus Function
1. Brush shape 實心空心切換
	
	首先宣告一個var filled來判斷目前是實心還空心，預設為實心，所以在初始化時將filled設定為true代表實心，如果按下按鈕則將filled設為相反的值，而brush shape會依據filled的值來決定是要填滿還是空心。
```
var filled;

window.onload = function(){
    filled = true;
}

function change_mode(value){
    if(value == "unfilled"){
        if(filled == true){
            filled = false;
        }
        else{
            filled = true;
        }
        return;
    }
    
}
```
2. 彩虹筆

首先先初始化：
```
// 紀錄原本的色彩
var prev_rgba;
// 紀錄目前彩虹筆畫到的顏色
var rgb_rainbow;
var rgb_flag;

window.onload = function(){
    prev_rgba = [0, 0, 0];
    rgb_rainbow = [255, 0, 0];
    rgb_flag = 0;
}
```
當滑鼠按下的時候，將顏色改為紅色並且記錄下目前的點，與畫第一點，與筆刷相似：
```
// if mouse down and mode is rainbow
function mouse_down_rainbow(mouse){
    rect = canvas.getBoundingClientRect();
    painting = true;
    let x = mouse.clientX - rect.left; 
    let y = mouse.clientY - rect.top;
    last_x = x;
    last_y = y;
    // draw a circle if mouse down
    ctx.save();
    ctx.beginPath();
    ctx.strokeStyle = "rgb(255,0,0)";
    ctx.fillStyle = "rgb(255,0,0)";
    ctx.arc(x, y, brush_size, 0, Math.PI * 2)
    ctx.fill();
    ctx.closePath();
}
```
當滑鼠移動時，在畫的同時持續對顏色rgb做調整，使其達到顏色像彩虹變換的效果，其餘功能與畫筆類似：
```
// if mouse move and mode is rainbow
function mouse_move_rainbow(mouse){
    if(painting == true){
        rect = canvas.getBoundingClientRect();
        let x = mouse.clientX - rect.left; 
        let y = mouse.clientY - rect.top;
        // calculate rgb
        if(rgb_flag < 125){
            rgb_rainbow[1]=rgb_rainbow[1]+2;
            rgb_flag++;
        }
        else if(rgb_flag < 250){
            rgb_rainbow[0]=rgb_rainbow[0]-2;
            rgb_flag++;
        }
        else if(rgb_flag < 375){
            rgb_rainbow[2]=rgb_rainbow[2]+2;
            rgb_flag++;
        }
        else if(rgb_flag < 500){
            rgb_rainbow[1]=rgb_rainbow[1]-2;
            rgb_flag++;
        }
        else if(rgb_flag < 625){
            rgb_rainbow[0]=rgb_rainbow[0]+2;
            rgb_flag++;
        }
        else if(rgb_flag < 750){
            rgb_rainbow[2]=rgb_rainbow[2]-2;
            rgb_flag++;
        }
        else{
            rgb_rainbow[0]=255;
            rgb_rainbow[1]=0;
            rgb_rainbow[2]=0;
            rgb_flag = 0;
        }
        ctx.strokeStyle = "rgb(" + rgb_rainbow[0] + ',' + rgb_rainbow[1] + ',' + rgb_rainbow[2] +")";
        ctx.fillStyle = "rgb(" + rgb_rainbow[0] + ',' + rgb_rainbow[1] + ',' + rgb_rainbow[2] +")";
        // draw a line if mouse move
        ctx.lineCap = "round";
        ctx.lineJoin = "round";
        ctx.beginPath();
        ctx.moveTo(last_x, last_y);
        ctx.lineTo(x, y);
        ctx.closePath();
        ctx.stroke();
        // save the mouse location
        last_x = x;
        last_y = y;
    }
}
```
當滑鼠結束按下時，將rainbow目前的var初始化並將畫筆顏色改回彩虹筆之前的顏色，並記錄下目前畫布的狀態：
```
//if mouse up and mode is rainbow
function mouse_up_rainbow(){
    painting = false;
    rgb_rainbow[0]=255;
    rgb_rainbow[1]=0;
    rgb_rainbow[2]=0;
    rgb_flag = 0;
    ctx.strokeStyle = "rgb(" + prev_rgba[0] + ',' + prev_rgba[1] + ',' + prev_rgba[2] +")";
    ctx.fillStyle = "rgb(" + prev_rgba[0] + ',' + prev_rgba[1] + ',' + prev_rgba[2] +")";
    un_re_idx++;
    if(un_re_idx < un_re_arr.length){
        un_re_arr.length = un_re_idx;
    }
    un_re_arr.push(canvas.toDataURL());
}
```

3. 清除選定矩形面積
	
	滑鼠按下以及移動的function與brush shape - square相同，皆為畫出矩形面積，且記錄初始位置以及不斷更新移動到的位置：	
	
```
var x_gt_sx, y_gt_sy;
var clear_x, clear_y;
var clear_offset_x, clear_offset_y;

// 滑鼠點下畫矩形
function mouse_down_rectclean(mouse){
    url = canvas.toDataURL();
    img.src = url;
    rect = canvas.getBoundingClientRect();
    painting = true;
    let x = mouse.clientX - rect.left; 
    let y = mouse.clientY - rect.top;
    s_x = x;
    s_y = y;
}

// 滑鼠移動畫矩形
function mouse_move_rectclean(mouse){
    if(painting == true){
        rect = canvas.getBoundingClientRect();
        clear_x = mouse.clientX - rect.left; 
        clear_y = mouse.clientY - rect.top;
        // calculate length and width
        clear_offset_x = (clear_x > s_x)? (clear_x - s_x) :(s_x - clear_x); 
        clear_offset_y = (clear_y > s_y)? (clear_y - s_y) :(s_y - clear_y);
        // determine square up or down
        if (clear_x > s_x)
            x_gt_sx = true;
        else
            x_gt_sx = false;
        if (clear_y > s_y)
            y_gt_sy = true;
        else
            y_gt_sy = false;
        // draw a circle if mouse move
        ctx.save();
        ctx.beginPath();
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
        
        if(x_gt_sx == true){
            if(y_gt_sy == true){
                ctx.fillRect(s_x, s_y, clear_offset_x, clear_offset_y);
            }
            else{
                ctx.fillRect(s_x, clear_y, clear_offset_x, clear_offset_y);
            }
        }
        else{
            if(y_gt_sy == true){
                ctx.fillRect(clear_x, s_y, clear_offset_x, clear_offset_y);
            }
            else{
                ctx.fillRect(clear_x, clear_y, clear_offset_x, clear_offset_y);
            }
        }
        ctx.closePath();
        // save the mouse location
    }
}
```
結束按下滑鼠時，停止畫矩形並且根據初始位置以及最後更新的滑鼠移動位置清除畫布上矩形面積區域，並將目前畫布的狀態記錄下來：
```
// 滑鼠UP停止畫矩形並消除區域
function mouse_up_rectclean(mouse){
    painting = false;
    if(x_gt_sx == true){
        if(y_gt_sy == true){
            ctx.clearRect(s_x, s_y, clear_offset_x, clear_offset_y);
        }
        else{
            ctx.clearRect(s_x, clear_y, clear_offset_x, clear_offset_y);
        }
    }
    else{
        if(y_gt_sy == true){
            ctx.clearRect(clear_x, s_y, clear_offset_x, clear_offset_y);
        }
        else{
            ctx.clearRect(clear_x, clear_y, clear_offset_x, clear_offset_y);
        }
    }
    un_re_idx++;
    if(un_re_idx < un_re_arr.length){
        un_re_arr.length = un_re_idx;
    }
    un_re_arr.push(canvas.toDataURL());
}
```

4. Brush shape - line shape
	
	當滑鼠按下時，記錄下目前初始的位置並畫一個點。
	滑鼠移動時，不斷更新目前移動到的位置並將初始點以及目前移動到的位置連線起來並畫出來。
	滑鼠結束按下時，結束畫線：
```
var l_x;
var l_y;

// LINE-滑鼠點下畫圓
function mouse_down_line(mouse){
    url = canvas.toDataURL();
    img.src = url;
    rect = canvas.getBoundingClientRect();
    painting = true;
    let x = mouse.clientX - rect.left; 
    let y = mouse.clientY - rect.top;
    l_x = x;
    l_y = y;
    // 點下去先畫一個圓
    ctx.save();
    ctx.beginPath();
    ctx.arc(x, y, 1, 0, Math.PI * 2);
    ctx.fill();
    ctx.closePath();
}

// 滑鼠移動畫線條
function mouse_move_line(mouse){
    if(painting == true){
        rect = canvas.getBoundingClientRect();
        let x = mouse.clientX - rect.left; 
        let y = mouse.clientY - rect.top;
        // draw a line if mouse move
        ctx.save();
        ctx.beginPath();
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img, 0, 0,canvas.width,canvas.height);
        ctx.moveTo(l_x, l_y);
        ctx.lineTo(x, y);
        ctx.stroke();
        ctx.closePath();
        // save the mouse location
    }
}
```

5. 漸層色方塊

	滑鼠點下時，先畫一個點並記錄初始位置
	滑鼠移動時，依據初始點以及目前位置去建立一個漸層色塊，並將畫筆顏色改為這個漸層色塊，並依據初始位置以及目前位置將這個色塊以矩形的方式畫出來，畫完後將顏色改回原本的顏色：
	
```
// 滑鼠點下畫漸層色
function mouse_down_gradient(mouse){
    url = canvas.toDataURL();
    img.src = url;
    rect = canvas.getBoundingClientRect();
    painting = true;
    let x = mouse.clientX - rect.left; 
    let y = mouse.clientY - rect.top;
    s_x = x;
    s_y = y;
    // 點下去先畫一個點
    ctx.save();
    ctx.beginPath();
    ctx.arc(x, y, 1, 0, Math.PI * 2)
    ctx.fill();
    ctx.closePath();
}

// 滑鼠移動畫漸層色矩形
function mouse_move_gradient(mouse){
    if(painting == true){
        rect = canvas.getBoundingClientRect();
        let x = mouse.clientX - rect.left; 
        let y = mouse.clientY - rect.top;
        // calculate length and width
        let offset_x = (x > s_x)? (x - s_x) :(s_x - x); 
        let offset_y = (y > s_y)? (y - s_y) :(s_y - y);
        // add gradient
        var gradientBar = ctx.createLinearGradient(s_x, s_y, x, y);
        gradientBar.addColorStop(0, "#ff0000");
        gradientBar.addColorStop(1 / 6, '#ff00ff');
        gradientBar.addColorStop(2 / 6, '#0000ff');
        gradientBar.addColorStop(3 / 6, '#00ffff');
        gradientBar.addColorStop(4 / 6, '#00ff00');
        gradientBar.addColorStop(5 / 6, '#ffff00');
        gradientBar.addColorStop(1, '#ff0000');

        ctx.fillStyle = gradientBar;
        // determine square up or down
        var x_gt_sx, y_gt_sy;
        if (x > s_x)
            x_gt_sx = true;
        else
            x_gt_sx = false;
        if (y > s_y)
            y_gt_sy = true;
        else
            y_gt_sy = false;
        // draw a circle if mouse move
        ctx.save();
        ctx.beginPath();
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
        if(x_gt_sx == true){
            if(y_gt_sy == true){
                ctx.fillRect(s_x, s_y, offset_x, offset_y);
            }
            else{
                ctx.fillRect(s_x, y, offset_x, offset_y);
            }
        }
        else{
            if(y_gt_sy == true){
                ctx.fillRect(x, s_y, offset_x, offset_y);
            }
            else{
                ctx.fillRect(x, y, offset_x, offset_y);
            }
        }
        ctx.fillStyle = "rgb(" + prev_rgba[0] + ',' + prev_rgba[1] + ',' + prev_rgba[2] +")";
        ctx.closePath();
        // save the mouse location
    }
}
``` 
 
---
### Gitlab page link
https://108062217.gitlab.io/AS_01_WebCanvas

---
### Others (Optional)


---

<style>
table th{
    width: 100%;
}
