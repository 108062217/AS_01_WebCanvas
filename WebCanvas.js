// canvas var
var canvas;
var ctx;
var rect;
// color_picker_canvas_var
var canvas_color;
var color_ctx;
var color_rect;
// 現在顏色
var cur_color;
// 目前RGB跟HEX
var rgb;
var hex;
// 判斷畫畫
var painting;
// 判斷橡皮擦
var era;
// 判斷模式
var mode;
// 紀錄滑鼠移動之前的x,y
var last_x;
var last_y;
// brush size
var brush_size;
// size range
var range;
// 文字內容
var text_box;
var text_content;
var text_flag;
var type_face;
var font_size;
// download form
var download_name;
var download_form;
// 實心或空心VAR
var filled;
// circle var
var r_x;
var r_y;
// URL
var url;
var img;
// triangle var
var t_x;
var t_y;
// square var
var s_x;
var s_y;
// undo redo array and idx
var un_re_idx;
var un_re_arr;
// rainbow var
var prev_rgba;
var rgb_rainbow;
var rgb_flag;
// line var
var l_x;
var l_y;


// 加載
window.onload = function(){
    // 初始化Canvas
    canvas = document.getElementById("my_canvas");
    ctx = canvas.getContext("2d");
    // 初始筆刷SIZE
    ctx.lineWidth = 2;
    // painting判斷是否畫畫
    painting = false;
    // era判斷是否橡皮擦
    era = false;
    // mode代表現在的畫筆
    mode = "none"
    // 筆刷大小 預設為1(radius)
    brush_size = 1;
    // size range
    range = document.getElementById("brush_range")
    // 初始化Color_picker_canvas
    canvas_color = document.getElementById("color_selector_canvas")
    color_ctx = canvas_color.getContext("2d");
    // GET VEX AND RGB and cur color
    cur_color = document.getElementById("cur_color");
    rgb = document.getElementById("rgb_in");
    hex = document.getElementById("hex_in");
    // 初始化生成Color_bar及Color_box
    color_bar();
    color_box("#ff0000");
    // 初始化 文字內容
    text_box = document.getElementById("textBox");
    text_content = "";
    text_flag = false;
    type_face = document.getElementById("typeface");
    font_size = document.getElementById("fontsize");
    // 初始化 download form
    download_name = document.getElementById("down_in");
    download_form = document.getElementById("dl_type");
    // 初始化 FILLED OR NOT
    filled = true;
    // 初始化URL
    url = "";
    img = new Image();
    // 初始化un_re_idx, arr
    un_re_arr = new Array();
    un_re_idx = -1;
    un_re_idx++;
    if(un_re_idx < un_re_arr.length){
        un_re_arr.length = un_re_idx;
    }
    un_re_arr.push(canvas.toDataURL());
    
    ctx.lineCap = "round";
    ctx.lineJoin = "round";

    prev_rgba = [0, 0, 0];
    rgb_rainbow = [255, 0, 0];
    rgb_flag = 0;
}

function mouse_enter(){
    canvas.style.cursor = "pointer";
    if(mode == 'brush'){
        url = 'img/cursor_brush.png'
        canvas.style.cursor = "url("+ url + "), auto";
    }
    else if(mode == 'eraser'){
        url = 'img/cursor_eraser.png'
        canvas.style.cursor = "url("+ url + "), auto";
    }
    else if(mode == "text"){
        canvas.style.cursor = "text";
    }
    else if(mode == "circle"){
        url = 'img/cursor_circle.png'
        canvas.style.cursor = "url("+ url + "), auto";
    }
    else if(mode == "square"){
        url = 'img/cursor_square.png'
        canvas.style.cursor = "url("+ url + "), auto";
    }
    else if(mode == "triangle"){
        url = 'img/cursor_triangle.png'
        canvas.style.cursor = "url("+ url + "), auto";
    }
    else if(mode == "rainbow"){
        url = 'img/cursor_rainbow.png'
        canvas.style.cursor = "url("+ url + "), auto";
    }
    else if(mode == "rect_clean"){
        url = 'img/cursor_square.png'
        canvas.style.cursor = "url("+ url + "), auto";
    }
    else if(mode == "line"){
        canvas.style.cursor = "crosshair";
    }
    else if(mode == 'gradient'){
        
        url = 'img/cursor_gradient.png'
        canvas.style.cursor = "url("+ url + "), auto";
    }
}

// 左側色相BAR
function color_bar() {
    var gradientBar = color_ctx.createLinearGradient(0, 0, 0, 160);
    gradientBar.addColorStop(0, "#ff0000");
    gradientBar.addColorStop(1 / 6, '#ff00ff');
    gradientBar.addColorStop(2 / 6, '#0000ff');
    gradientBar.addColorStop(3 / 6, '#00ffff');
    gradientBar.addColorStop(4 / 6, '#00ff00');
    gradientBar.addColorStop(5 / 6, '#ffff00');
    gradientBar.addColorStop(1, '#ff0000');

    color_ctx.fillStyle = gradientBar;
    color_ctx.fillRect(0, 0, 20, 160);
}
// 中間顏色選擇
function color_box(color){
    // 底色填充(橫向)，從白色到色相選定顏色
    var gradientBase = color_ctx.createLinearGradient(30, 0, 160 + 30, 0);
    gradientBase.addColorStop(1, color);
    gradientBase.addColorStop(0, 'rgba(255,255,255,1)');
    color_ctx.fillStyle = gradientBase;
    color_ctx.fillRect(30, 0, 160 + 30, 160);
    
    // 第二次填充(縱向)，透明到黑色
    var gradient_down = color_ctx.createLinearGradient(0, 0, 0, 160);
    gradient_down.addColorStop(0, 'rgba(0,0,0,0)');
    gradient_down.addColorStop(1, 'rgba(0,0,0,1)');
    color_ctx.fillStyle = gradient_down;
    color_ctx.fillRect(30, 0, 160 + 30, 160);
}
// 點選顏色 透過Event Listener - Onclick
function color_choose(mouse){
    color_rect = canvas_color.getBoundingClientRect();
    let x = mouse.clientX - color_rect.left;
    let y = mouse.clientY - color_rect.top;
    var rgbaStr = '#000000';
    if (x >= 0 && x< 20 && y >= 0 && y < 160) {
        // in left color bar
        rgbaStr = getRgbaAtPoint(x, y, 'bar');
        color_box('rgba(' + rgbaStr + ')');
    }
    else if(x >= 30 && x< 30 + 160 && y >=0 && y < 160){
        rgbaStr = getRgbaAtPoint(x, y, 'box');
    }
    else{
        return;
    }
    // 設定現在顏色到var
    prev_rgba[0] = rgbaStr[0];
    prev_rgba[1] = rgbaStr[1];
    prev_rgba[2] = rgbaStr[2];
    // set 選擇框
    cur_color.style.left = x + color_rect.left + 'px';
    cur_color.style.top = y + color_rect.top + 'px';
    cur_color.style.outlineColor = (rgbaStr[0] > 256 / 2 || rgbaStr[1] > 256 / 2 || rgbaStr[2] > 256 / 2) ? '#000' : '#fff';
    // output rgb and hex
    var hex_val = out_rgb_and_hex(rgbaStr);
    // 改變畫筆顏色
    ctx.strokeStyle =  hex_val;
    ctx.fillStyle = hex_val;
}
// 根據選到的點決定顏色
function getRgbaAtPoint(x, y, area){
    var imgData;
    if(area == 'bar'){
        imgData = color_ctx.getImageData(0, 0, 20, 160);
    }
    else{
        imgData = color_ctx.getImageData(0, 0, canvas_color.width, canvas_color.height);
    }
    var image_buffer = imgData.data;
    var index = (y * imgData.width + x) * 4;
    return[
        image_buffer[index], 
        image_buffer[index + 1], 
        image_buffer[index + 2] , 
        (image_buffer[index + 3]/255).toFixed(2),
    ]
}
// output rgb and hex
function out_rgb_and_hex(rgb_in){
    rgb.value = rgb_in[0] + ', ' + rgb_in[1] + ', ' + rgb_in[2];
    
    hex.value = '#' + (rgb_in[0].toString(16).length == 1? '0' + rgb_in[0].toString(16): rgb_in[0].toString(16));
    hex.value = hex.value + (rgb_in[1].toString(16).length == 1? '0' + rgb_in[1].toString(16): rgb_in[1].toString(16)); 
    hex.value = hex.value + (rgb_in[2].toString(16).length == 1? '0' + rgb_in[2].toString(16): rgb_in[2].toString(16)); 
    return hex.value;
}

// 按下mode button 決定mode
function change_mode(value){
    if(value == "refresh"){
        refresh_all();
        return;
    }
    else if(value == "download"){
        download_img();
        return;
    }
    else if(value == "upload"){
        upload_img();
        return;
    }
    else if(value == "unfilled"){
        if(filled == true){
            filled = false;
        }
        else{
            filled = true;
        }
        return;
    }
    else if(value == "undo"){
        un_do();
        return;
    }
    else if(value == "redo"){
        re_do();
        return;
    }
    mode = value;
}
// Event Listener - mouse down
function mouse_down(mouse){
    if(mode == 'brush'){
        mouse_down_brush(mouse);
    }
    else if(mode == "eraser"){
        mouse_down_eraser(mouse);
    }
    else if(mode == "text"){
        mouse_down_text(mouse);
    }
    else if(mode == "circle"){
        mouse_down_circle(mouse);
    }
    else if(mode == "triangle"){
        mouse_down_triangle(mouse);
    }
    else if(mode == "square"){
        mouse_down_square(mouse);
    }
    else if(mode == "rainbow"){
        mouse_down_rainbow(mouse);
    }
    else if(mode == "rect_clean"){
        mouse_down_rectclean(mouse);
    }
    else if(mode == "line"){
        mouse_down_line(mouse);
    }
    else if(mode == "gradient"){
        mouse_down_gradient(mouse);
    }
}
// Event Listener - mouse move
function mouse_move(mouse){
    if(mode == 'brush'){
        mouse_move_brush(mouse);
    }
    else if(mode == 'eraser'){
        mouse_move_eraser(mouse);
    }
    else if(mode == 'circle'){
        mouse_move_circle(mouse);
    }
    else if(mode == 'triangle'){
        mouse_move_triangle(mouse);
    }
    else if(mode == 'square'){
        mouse_move_square(mouse);
    }
    else if(mode == 'rainbow'){
        mouse_move_rainbow(mouse);
    }
    else if(mode == "rect_clean"){
        mouse_move_rectclean(mouse);
    }
    else if(mode == "line"){
        mouse_move_line(mouse);
    }
    else if(mode == "gradient"){
        mouse_move_gradient(mouse);
    }
}
// Event Listener - mouse up
function mouse_up(){
    if(mode == 'brush'){
        mouse_up_brush();
    }
    else if(mode == 'eraser'){
        mouse_up_brush();
    }
    else if(mode == 'circle'){
        mouse_up_brush();
    }
    else if(mode == 'triangle'){
        mouse_up_brush();
    }
    else if(mode == 'square'){
        mouse_up_brush();
    }
    else if(mode == 'rainbow'){
        mouse_up_rainbow();
    }
    else if(mode == "rect_clean"){
        mouse_up_rectclean();
    }
    else if(mode == "line"){
        mouse_up_brush();
    }
    else if(mode == "gradient"){
        mouse_up_brush();
    }
}
// if mouse down and mode is brush
function mouse_down_brush(mouse){
    rect = canvas.getBoundingClientRect();
    painting = true;
    let x = mouse.clientX - rect.left; 
    let y = mouse.clientY - rect.top;
    last_x = x;
    last_y = y;
    // draw a circle if mouse down
    ctx.save();
    ctx.beginPath();
    ctx.arc(x, y, brush_size, 0, Math.PI * 2)
    ctx.fill();
    ctx.closePath();
}
// if mouse move and mode is brush
function mouse_move_brush(mouse){
    if(painting == true){
        rect = canvas.getBoundingClientRect();
        let x = mouse.clientX - rect.left; 
        let y = mouse.clientY - rect.top;
        // draw a line if mouse move
        ctx.lineCap = "round";
        ctx.lineJoin = "round";
        ctx.beginPath();
        ctx.moveTo(last_x, last_y);
        ctx.lineTo(x, y);
        ctx.closePath();
        ctx.stroke();
        // save the mouse location
        last_x = x;
        last_y = y;
    }
}
//if mouse up and mode is brush
function mouse_up_brush(){
    painting = false;
    un_re_idx++;
    if(un_re_idx < un_re_arr.length){
        un_re_arr.length = un_re_idx;
    }
    un_re_arr.push(canvas.toDataURL());
}
// change brush size if range slider is move
function change_size(){
    ctx.lineWidth = range.value;
    brush_size = ctx.lineWidth/2;
}
// mouse down and mode is eraser
function mouse_down_eraser(mouse){
    rect = canvas.getBoundingClientRect();
    painting = true;
    let x = mouse.clientX - rect.left; 
    let y = mouse.clientY - rect.top;
    last_x = x;
    last_y = y;
    // clear if mouse down
    ctx.save();
    ctx.beginPath();
    ctx.arc(x, y, brush_size, 0, Math.PI * 2);
    ctx.clip();
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.restore();
}
// mouse move and mode is eraser
function mouse_move_eraser(mouse){
    if(painting == true){
        rect = canvas.getBoundingClientRect();
        let x = mouse.clientX - rect.left; 
        let y = mouse.clientY - rect.top;
        // calculate 中間區域
        var asin = brush_size*Math.sin(Math.atan((y-last_y)/(x-last_x)));
        var acos = brush_size*Math.cos(Math.atan((y-last_y)/(x-last_x)));
        var x3 = last_x + asin;
        var y3 = last_y - acos;
        var x4 = last_x - asin;
        var y4 = last_y + acos;
        var x5 = x+asin;
        var y5 = y-acos;
        var x6 = x-asin;
        var y6 = y+acos;
        // clear if mouse move
        ctx.save();
        ctx.globalCompositeOperation = "destination-out";
        ctx.beginPath();
        ctx.arc(x, y, brush_size, 0, Math.PI * 2);
        ctx.clip();
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.restore();
        // clear the rect 
        //清除矩形剪辑区域里的像素
        ctx.save()
        ctx.beginPath()
        ctx.moveTo(x3,y3);
        ctx.lineTo(x5,y5);
        ctx.lineTo(x6,y6);
        ctx.lineTo(x4,y4);
        ctx.closePath();
        ctx.clip()
        ctx.clearRect(0,0,canvas.width,canvas.height);
        ctx.restore();
        // save the mouse location
        last_x = x;
        last_y = y;
    }
}

// mouse down and mode is text
function mouse_down_text(mouse){
    // 按下時分別可調出和完成文字的填寫。
    let x, y;
    if(text_flag == true){
    //當前為輸入狀態，點擊canvas等於完成填寫，這時需要隱藏輸入欄，並且獲取內容，將其繪入canvas中。
        text_content = text_box.value;
        text_flag = false;
        text_box.style['z-index'] = 1;
        text_box.value = "";
        txt_drawing(x, y);
        un_re_idx++;
        if(un_re_idx < un_re_arr.length){
            un_re_arr.length = un_re_idx;
        }
        un_re_arr.push(canvas.toDataURL());
    }
    else{
    //當前為非輸入狀態，將輸入框調至最前，開始輸入
        rect = canvas.getBoundingClientRect();
        x = mouse.clientX - rect.left; 
        y = mouse.clientY - rect.top;
        text_box.style.left = (x + 10) > 295? 295 + 'px': (x + 10 + 'px');
        text_box.style.top = (y + 180) > 500? 500 + 'px' : (y + 180 + 'px');
        text_flag = true;
        text_box.style['z-index'] = 6;
    }
}
// draw the txt onto canvas
function txt_drawing(x, y){
    rect = canvas.getBoundingClientRect();
    ctx.font = font_size.value + " " + type_face.value;
    ctx.save();
    ctx.beginPath();
    ctx.fillText(text_content, parseInt(text_box.style.left), parseInt(text_box.style.top) - 150);
    ctx.closePath();
    ctx.restore();
}

// refresh button is clicked
function refresh_all(){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    un_re_idx++;
    if(un_re_idx < un_re_arr.length){
        un_re_arr.length = un_re_idx;
    }
    un_re_arr.push(canvas.toDataURL());
}

// download img
function download_img(){
    var link = document.createElement('a');
    link.download = download_name.value + download_form.value;
    link.href = canvas.toDataURL();
    link.click();
}

// 創建一個input用event讀file
function upload_img(){
    var input_object = document.createElement('input');
    input_object.addEventListener('change', readFile, false);
    input_object.type = 'file';
    input_object.accept = 'imag/*';
    input_object.id = 'input_object';
    input_object.click();
}
// 讀取圖片- 判斷是否為影像檔
function readFile(){
    var file = this.files[0];
    if(!/image\/\w+/.test(file.type)){
        alert("請確保檔案為影像類別")
        return false;
    }
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function(e){
        draw_to_canvas(this.result);
    }
}
// 將圖片畫到canvas上
function draw_to_canvas(imgData){
    var dl_img = new Image;
    dl_img.src = imgData;
    dl_img.onload = function(){
        ctx.drawImage(dl_img, 0, 0, canvas.width, canvas.height);
    }
}

// 滑鼠點下畫圓
function mouse_down_circle(mouse){
    url = canvas.toDataURL();
    img.src = url;
    rect = canvas.getBoundingClientRect();
    painting = true;
    let x = mouse.clientX - rect.left; 
    let y = mouse.clientY - rect.top;
    r_x = x;
    r_y = y;
    // 點下去先畫一個圓
    ctx.save();
    ctx.beginPath();
    ctx.arc(x, y, 1, 0, Math.PI * 2)
    ctx.fill();
    ctx.closePath();
}
// 滑鼠移動畫圓
function mouse_move_circle(mouse){
    if(painting == true){
        rect = canvas.getBoundingClientRect();
        let x = mouse.clientX - rect.left; 
        let y = mouse.clientY - rect.top;
        // calculate radius
        let offset_x = (x > r_x)? (x - r_x) :(r_x - x); 
        let offset_y = (y > r_y)? (y - r_y) :(r_y - y);
        let rad = Math.hypot(offset_x, offset_y);
        // draw a circle if mouse move
        ctx.save();
        ctx.beginPath();
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img, 0, 0,canvas.width,canvas.height);
        ctx.arc((r_x + x)/2, (r_y + y)/2, rad/2, 0, Math.PI * 2)
        if(filled)
            ctx.fill();
        else
            ctx.stroke();
        ctx.closePath();
        // save the mouse location
    }
}
// 滑鼠UP畫圓
function mouse_up_circle(mouse){
    painting = false;
    url = canvas.toDataURL();
    un_re_arr.push(url);
    un_re_idx = un_re_idx + 1;
}

// 滑鼠點下畫三角形
function mouse_down_triangle(mouse){
    url = canvas.toDataURL();
    img.src = url;
    rect = canvas.getBoundingClientRect();
    painting = true;
    let x = mouse.clientX - rect.left; 
    let y = mouse.clientY - rect.top;
    t_x = x;
    t_y = y;
    // 點下去先畫一個點
    ctx.save();
    ctx.beginPath();
    ctx.arc(x, y, 1, 0, Math.PI * 2)
    ctx.fill();
    ctx.closePath();
}
// 滑鼠移動畫三角形
function mouse_move_triangle(mouse){
    if(painting == true){
        rect = canvas.getBoundingClientRect();
        let x = mouse.clientX - rect.left; 
        let y = mouse.clientY - rect.top;
        // calculate triangle upper point
        let offset_x = (x + t_x) / 2;
        // draw a circle if mouse move
        ctx.save();
        ctx.beginPath();
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
        ctx.moveTo(offset_x, t_y);
        ctx.lineTo(x, y);
        ctx.lineTo(t_x, y);
        if(filled)
            ctx.fill();
        else{
            ctx.lineTo(offset_x, t_y);
            ctx.stroke();
            ctx.closePath();
        }
        ctx.closePath();
        // save the mouse location
    }
}
// 滑鼠UP停止畫三角形
function mouse_up_triangle(mouse){
    painting = false;
    url = canvas.toDataURL();
    un_re_arr.push(url);
    un_re_idx = un_re_idx + 1;
}

// 滑鼠點下畫矩形
function mouse_down_square(mouse){
    url = canvas.toDataURL();
    img.src = url;
    rect = canvas.getBoundingClientRect();
    painting = true;
    let x = mouse.clientX - rect.left; 
    let y = mouse.clientY - rect.top;
    s_x = x;
    s_y = y;
    // 點下去先畫一個點
    ctx.save();
    ctx.beginPath();
    ctx.arc(x, y, 1, 0, Math.PI * 2)
    ctx.fill();
    ctx.closePath();
}
// 滑鼠移動畫矩形
function mouse_move_square(mouse){
    if(painting == true){
        rect = canvas.getBoundingClientRect();
        let x = mouse.clientX - rect.left; 
        let y = mouse.clientY - rect.top;
        // calculate length and width
        let offset_x = (x > s_x)? (x - s_x) :(s_x - x); 
        let offset_y = (y > s_y)? (y - s_y) :(s_y - y);
        // determine square up or down
        var x_gt_sx, y_gt_sy;
        if (x > s_x)
            x_gt_sx = true;
        else
            x_gt_sx = false;
        if (y > s_y)
            y_gt_sy = true;
        else
            y_gt_sy = false;
        // draw a circle if mouse move
        ctx.save();
        ctx.beginPath();
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
        if(filled){
            if(x_gt_sx == true){
                if(y_gt_sy == true){
                    ctx.fillRect(s_x, s_y, offset_x, offset_y);
                }
                else{
                    ctx.fillRect(s_x, y, offset_x, offset_y);
                }
            }
            else{
                if(y_gt_sy == true){
                    ctx.fillRect(x, s_y, offset_x, offset_y);
                }
                else{
                    ctx.fillRect(x, y, offset_x, offset_y);
                }
            }
        }
        else{
            if(x_gt_sx == true){
                if(y_gt_sy == true){
                    ctx.strokeRect(s_x, s_y, offset_x, offset_y);
                }
                else{
                    ctx.strokeRect(s_x, y, offset_x, offset_y);
                }
            }
            else{
                if(y_gt_sy == true){
                    ctx.strokeRect(x, s_y, offset_x, offset_y);
                }
                else{
                    ctx.strokeRect(x, y, offset_x, offset_y);
                }
            }
        }
        ctx.closePath();
        // save the mouse location
    }
}

// undo func
function un_do(){
    var ctx = canvas.getContext("2d");
    if(un_re_idx == 0)
        return;
    un_re_idx--;
    var img = new Image();
    img.src = un_re_arr[un_re_idx];
    img.onload = function(){
        ctx.save();
        ctx.beginPath();
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
        ctx.closePath();
    }
}

// redo func
function re_do(){
    var ctx = canvas.getContext("2d");
    if(un_re_idx >= un_re_arr.length - 1)
        return;
    un_re_idx++;
    var img = new Image();
    img.src = un_re_arr[un_re_idx];
    img.onload = function(){
        ctx.save();
        ctx.beginPath();
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
        ctx.closePath();
    }
}

// if mouse down and mode is rainbow
function mouse_down_rainbow(mouse){
    rect = canvas.getBoundingClientRect();
    painting = true;
    let x = mouse.clientX - rect.left; 
    let y = mouse.clientY - rect.top;
    last_x = x;
    last_y = y;
    // draw a circle if mouse down
    ctx.save();
    ctx.beginPath();
    ctx.strokeStyle = "rgb(255,0,0)";
    ctx.fillStyle = "rgb(255,0,0)";
    ctx.arc(x, y, brush_size, 0, Math.PI * 2)
    ctx.fill();
    ctx.closePath();
}

// if mouse move and mode is brush
function mouse_move_rainbow(mouse){
    if(painting == true){
        rect = canvas.getBoundingClientRect();
        let x = mouse.clientX - rect.left; 
        let y = mouse.clientY - rect.top;
        // calculate rgb
        if(rgb_flag < 125){
            rgb_rainbow[1]=rgb_rainbow[1]+2;
            rgb_flag++;
        }
        else if(rgb_flag < 250){
            rgb_rainbow[0]=rgb_rainbow[0]-2;
            rgb_flag++;
        }
        else if(rgb_flag < 375){
            rgb_rainbow[2]=rgb_rainbow[2]+2;
            rgb_flag++;
        }
        else if(rgb_flag < 500){
            rgb_rainbow[1]=rgb_rainbow[1]-2;
            rgb_flag++;
        }
        else if(rgb_flag < 625){
            rgb_rainbow[0]=rgb_rainbow[0]+2;
            rgb_flag++;
        }
        else if(rgb_flag < 750){
            rgb_rainbow[2]=rgb_rainbow[2]-2;
            rgb_flag++;
        }
        else{
            rgb_rainbow[0]=255;
            rgb_rainbow[1]=0;
            rgb_rainbow[2]=0;
            rgb_flag = 0;
        }
        ctx.strokeStyle = "rgb(" + rgb_rainbow[0] + ',' + rgb_rainbow[1] + ',' + rgb_rainbow[2] +")";
        ctx.fillStyle = "rgb(" + rgb_rainbow[0] + ',' + rgb_rainbow[1] + ',' + rgb_rainbow[2] +")";
        // draw a line if mouse move
        ctx.lineCap = "round";
        ctx.lineJoin = "round";
        ctx.beginPath();
        ctx.moveTo(last_x, last_y);
        ctx.lineTo(x, y);
        ctx.closePath();
        ctx.stroke();
        // save the mouse location
        last_x = x;
        last_y = y;
    }
}
//if mouse up and mode is brush
function mouse_up_rainbow(){
    painting = false;
    rgb_rainbow[0]=255;
    rgb_rainbow[1]=0;
    rgb_rainbow[2]=0;
    rgb_flag = 0;
    ctx.strokeStyle = "rgb(" + prev_rgba[0] + ',' + prev_rgba[1] + ',' + prev_rgba[2] +")";
    ctx.fillStyle = "rgb(" + prev_rgba[0] + ',' + prev_rgba[1] + ',' + prev_rgba[2] +")";
    un_re_idx++;
    if(un_re_idx < un_re_arr.length){
        un_re_arr.length = un_re_idx;
    }
    un_re_arr.push(canvas.toDataURL());
}

// 滑鼠點下畫矩形
function mouse_down_rectclean(mouse){
    url = canvas.toDataURL();
    img.src = url;
    rect = canvas.getBoundingClientRect();
    painting = true;
    let x = mouse.clientX - rect.left; 
    let y = mouse.clientY - rect.top;
    s_x = x;
    s_y = y;
    // 點下去先畫一個點
}

var x_gt_sx, y_gt_sy;
var clear_x, clear_y;
var clear_offset_x, clear_offset_y;
// 滑鼠移動畫矩形
function mouse_move_rectclean(mouse){
    if(painting == true){
        rect = canvas.getBoundingClientRect();
        clear_x = mouse.clientX - rect.left; 
        clear_y = mouse.clientY - rect.top;
        // calculate length and width
        clear_offset_x = (clear_x > s_x)? (clear_x - s_x) :(s_x - clear_x); 
        clear_offset_y = (clear_y > s_y)? (clear_y - s_y) :(s_y - clear_y);
        // determine square up or down
        if (clear_x > s_x)
            x_gt_sx = true;
        else
            x_gt_sx = false;
        if (clear_y > s_y)
            y_gt_sy = true;
        else
            y_gt_sy = false;
        // draw a circle if mouse move
        ctx.save();
        ctx.beginPath();
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
        
        if(x_gt_sx == true){
            if(y_gt_sy == true){
                ctx.fillRect(s_x, s_y, clear_offset_x, clear_offset_y);
            }
            else{
                ctx.fillRect(s_x, clear_y, clear_offset_x, clear_offset_y);
            }
        }
        else{
            if(y_gt_sy == true){
                ctx.fillRect(clear_x, s_y, clear_offset_x, clear_offset_y);
            }
            else{
                ctx.fillRect(clear_x, clear_y, clear_offset_x, clear_offset_y);
            }
        }
        ctx.closePath();
        // save the mouse location
    }
}
// 滑鼠UP停止畫矩形並消除區域
function mouse_up_rectclean(mouse){
    painting = false;
    if(x_gt_sx == true){
        if(y_gt_sy == true){
            ctx.clearRect(s_x, s_y, clear_offset_x, clear_offset_y);
        }
        else{
            ctx.clearRect(s_x, clear_y, clear_offset_x, clear_offset_y);
        }
    }
    else{
        if(y_gt_sy == true){
            ctx.clearRect(clear_x, s_y, clear_offset_x, clear_offset_y);
        }
        else{
            ctx.clearRect(clear_x, clear_y, clear_offset_x, clear_offset_y);
        }
    }
    un_re_idx++;
    if(un_re_idx < un_re_arr.length){
        un_re_arr.length = un_re_idx;
    }
    un_re_arr.push(canvas.toDataURL());
}

// LINE-滑鼠點下畫圓
function mouse_down_line(mouse){
    url = canvas.toDataURL();
    img.src = url;
    rect = canvas.getBoundingClientRect();
    painting = true;
    let x = mouse.clientX - rect.left; 
    let y = mouse.clientY - rect.top;
    l_x = x;
    l_y = y;
    // 點下去先畫一個圓
    ctx.save();
    ctx.beginPath();
    ctx.arc(x, y, 1, 0, Math.PI * 2);
    ctx.fill();
    ctx.closePath();
}
// 滑鼠移動畫線條
function mouse_move_line(mouse){
    if(painting == true){
        rect = canvas.getBoundingClientRect();
        let x = mouse.clientX - rect.left; 
        let y = mouse.clientY - rect.top;
        // draw a line if mouse move
        ctx.save();
        ctx.beginPath();
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img, 0, 0,canvas.width,canvas.height);
        ctx.moveTo(l_x, l_y);
        ctx.lineTo(x, y);
        ctx.stroke();
        ctx.closePath();
        // save the mouse location
    }
}

// 滑鼠點下畫漸層色
function mouse_down_gradient(mouse){
    url = canvas.toDataURL();
    img.src = url;
    rect = canvas.getBoundingClientRect();
    painting = true;
    let x = mouse.clientX - rect.left; 
    let y = mouse.clientY - rect.top;
    s_x = x;
    s_y = y;
    // 點下去先畫一個點
    ctx.save();
    ctx.beginPath();
    ctx.arc(x, y, 1, 0, Math.PI * 2)
    ctx.fill();
    ctx.closePath();
}
// 滑鼠移動畫漸層色矩形
function mouse_move_gradient(mouse){
    if(painting == true){
        rect = canvas.getBoundingClientRect();
        let x = mouse.clientX - rect.left; 
        let y = mouse.clientY - rect.top;
        // calculate length and width
        let offset_x = (x > s_x)? (x - s_x) :(s_x - x); 
        let offset_y = (y > s_y)? (y - s_y) :(s_y - y);
        // add gradient
        var gradientBar = ctx.createLinearGradient(s_x, s_y, x, y);
        gradientBar.addColorStop(0, "#ff0000");
        gradientBar.addColorStop(1 / 6, '#ff00ff');
        gradientBar.addColorStop(2 / 6, '#0000ff');
        gradientBar.addColorStop(3 / 6, '#00ffff');
        gradientBar.addColorStop(4 / 6, '#00ff00');
        gradientBar.addColorStop(5 / 6, '#ffff00');
        gradientBar.addColorStop(1, '#ff0000');

        ctx.fillStyle = gradientBar;
        // determine square up or down
        var x_gt_sx, y_gt_sy;
        if (x > s_x)
            x_gt_sx = true;
        else
            x_gt_sx = false;
        if (y > s_y)
            y_gt_sy = true;
        else
            y_gt_sy = false;
        // draw a circle if mouse move
        ctx.save();
        ctx.beginPath();
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
        if(x_gt_sx == true){
            if(y_gt_sy == true){
                ctx.fillRect(s_x, s_y, offset_x, offset_y);
            }
            else{
                ctx.fillRect(s_x, y, offset_x, offset_y);
            }
        }
        else{
            if(y_gt_sy == true){
                ctx.fillRect(x, s_y, offset_x, offset_y);
            }
            else{
                ctx.fillRect(x, y, offset_x, offset_y);
            }
        }
        ctx.fillStyle = "rgb(" + prev_rgba[0] + ',' + prev_rgba[1] + ',' + prev_rgba[2] +")";
        ctx.closePath();
        // save the mouse location
    }
}